const path = require('path');
const webpack = require("webpack");
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractCSS = new ExtractTextPlugin('styles.min.css');

module.exports = {
	devtool: 'cheap-eval-source-map',
	entry: {
		path: path.resolve(__dirname, 'src')
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		filename: 'main.min.js',
		publicPath: "/"
	},
	module: {
		rules: [
			{
				test: /\.js$|\.es6|\.jsx$/,
				resolve: {
					extensions: ['.es6', '.js', '.jsx']
				},
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.css$|\.scss$/,
				resolve: {
					extensions: ['.css', '.scss']
				},
				use: extractCSS.extract({
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader'
						},
						{
							loader: 'postcss-loader',
							options: {
								sourceMap: true
							}
						}
					],
				})
			},
			{
				test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'fonts'+path.sep
					}
				}]
			},
			{
				test: /\.png$/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'images'+path.sep
					}
				}]
			},
			{
				test: /\.jpg/,
				use: [{
					loader: 'file-loader',
					options: {
						name: '[name].[ext]',
						outputPath: 'images'+path.sep
					}
				}]
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: path.resolve('src', 'index.html')
		}),
		extractCSS
	]
};


