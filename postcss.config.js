module.exports = {
	parser: 'postcss-scss',
	plugins: {
		'autoprefixer': {},
		'cssnano': {},
		'postcss-import': {},
		'postcss-nested': {},
		'postcss-mixins': {},
		'postcss-simple-vars': {}
	}
};