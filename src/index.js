import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import { hot } from 'react-hot-loader'
import { BrowserRouter as Router } from 'react-router-dom';

import store from 'config/redux';
import App from "core/App";

const AppComponent = () => (
	<React.Fragment>
		<App/>
	</React.Fragment>
);

ReactDOM.render(
<Provider store = {store}>
	<Router>
	<AppComponent/>
	</Router>
	</Provider>,
document.getElementById('root')
);

export default hot(module)(AppComponent);

if (module.hot) {
	const hotEmitter = require("webpack/hot/emitter");
	const DEAD_CSS_TIMEOUT = 2000;
	
	hotEmitter.on("webpackHotUpdate", function(currentHash) {
		document.querySelectorAll("link[href][rel=stylesheet]").forEach((link) => {
			const nextStyleHref = link.href.replace(/(\?\d+)?$/, `?${Date.now()}`);
			const newLink = link.cloneNode();
			newLink.href = nextStyleHref;
			
			link.parentNode.appendChild(newLink);
			setTimeout(() => {
				if (link.parentNode !== null)
					link.parentNode.removeChild(link);
			}, DEAD_CSS_TIMEOUT);
		});
	})
}
