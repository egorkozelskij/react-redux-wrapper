import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import {testReducer} from 'section/Test';


const rootReducer = combineReducers({
	test: testReducer
});

let store = createStore(
	rootReducer,
	composeWithDevTools(
		applyMiddleware(thunk)
	)
);

export default store;