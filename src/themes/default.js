import createMuiTheme from "@material-ui/core/es/styles/createMuiTheme";

const theme = createMuiTheme(
	{
		typography: {
			useNextVariants: true,
		},
		"palette":{
			"common":{
				"black":"rgba(11, 11, 11, 1)",
				"white":"#fff"
			},
			"background":{
				"paper":"#fff",
				"default":"#fafafa"
			},
			"primary":{
				"light":"#2984cb",
				"main":"#16b589",
				"dark":"#303f9f",
				"contrastText":"#fff"
			},
			"secondary":{
				"light":"rgba(177, 101, 125, 1)",
				"main":"rgba(40, 67, 107, 1)",
				"dark":"#c51162",
				"contrastText":"#fff"
			},
			"error":{
				"light":"#e57373",
				"main":"#f44336",
				"dark":"#d32f2f",
				"contrastText":"#fff"
			},
			"text":{
				"primary":"rgba(0, 0, 0, 0.87)",
				"secondary":"rgba(0, 0, 0, 0.54)",
				"disabled":"rgba(0, 0, 0, 0.38)",
				"hint":"rgba(0, 0, 0, 0.38)"
			}
		}
	}
);
export default theme;