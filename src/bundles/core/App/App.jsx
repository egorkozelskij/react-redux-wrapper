import React, { Component } from 'react';
import jssPreset from "@material-ui/core/es/styles/jssPreset";
import JssProvider from 'react-jss/lib/JssProvider';
import createGenerateClassName from "@material-ui/core/es/styles/createGenerateClassName";
import {MuiThemeProvider} from "@material-ui/core/es/styles/index";
import {CssBaseline} from "@material-ui/core/es/index";
import { create } from 'jss';
import theme from '/themes/default';
import globalStyles from './styles';
import withStyles from "@material-ui/core/es/styles/withStyles";

import Test from "section/Test";

const jss = create(jssPreset());
const generateClassName = createGenerateClassName({
	dangerouslyUseGlobalCSS: true,
	productionPrefix: 'c',
});

class App extends Component {
	constructor(props) {
		super(props);
	}
	
	componentDidMount() {
	}
	
    render() {
        return (
	        <JssProvider jss={jss} generateClassName={generateClassName}>
		        <MuiThemeProvider theme={theme}>
			        <CssBaseline />
					<Test/>
		        </MuiThemeProvider>
	        </JssProvider>
        );
    }
}
export default withStyles(globalStyles)(App);
