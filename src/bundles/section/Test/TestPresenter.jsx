import React from 'react';
import PropTypes from 'prop-types';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/es/Button/Button";
import AddIcon from '@material-ui/icons/Add';
import List from "@material-ui/core/es/List/List";
import ListItem from "@material-ui/core/es/ListItem/ListItem";
import {FormControl, InputLabel, MenuItem, Select} from "@material-ui/core/es/index";
import withStyles from "@material-ui/core/es/styles/withStyles";

import './style/style1.css';
import './style/style2.scss';

import styles from './style/style3';
import {connect} from "react-redux";
const TestPresenter = ({onChange, onSubmit, reducer, classes}) => (
	<React.Fragment>
		<h1>Hello from Test component</h1>
		<Typography variant="h6" gutterBottom>
			Add item to right list
		</Typography>
		<Grid container>
			<Grid item xs={6}>
				<Grid container spacing={8}>
					<Grid item xs={10}>
						<TextField
							required
							label="Item name"
							fullWidth
							onChange={onChange}
						/>
					</Grid>
					<Grid item xs={2}>
						<Button
							className={classes.myButton}
							variant="contained"
							color="primary"
							onClick={onSubmit}
							onSubmit={onSubmit}>
							<AddIcon/>
						</Button>
					</Grid>
				</Grid>
			</Grid>
			<Grid item xs={6}>
				{ reducer.isFetching ?
					<h4>Обновление списка...</h4>
					:
					<List>
						{reducer.data.map((item,key)  => <ListItem key={key}>{item}</ListItem>)}
					</List>
				}
			</Grid>
		</Grid>
	</React.Fragment>
);
TestPresenter.propTypes = {
	onChange: PropTypes.func.isRequired,
	onSubmit: PropTypes.func.isRequired,
	reducer: PropTypes.shape({
		data: PropTypes.arrayOf(PropTypes.string),
		isFetching: PropTypes.bool.isRequired
	}).isRequired,
};
const mapStateToProps = (state) => {
	return {
	};
};
export default withStyles(styles)(connect(mapStateToProps, null)(TestPresenter));
