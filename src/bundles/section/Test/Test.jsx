import React, { Component } from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import PropTypes from 'prop-types';

import TestPresenter from "./TestPresenter";
import {addNewItem} from './logic/operation';


class Test extends Component {
	constructor(props) {
		super(props);
		this.itemName = '';
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}
	
	onChange(e) {
		this.itemName = e.target.value;
	}
	
	onSubmit(e) {
		e.preventDefault();
		if (this.itemName  && this.itemName !== '')
			this.props.addNewItem(this.itemName);
	}
	
	render() {
        return (
             <TestPresenter onChange={this.onChange}
                            onSubmit={this.onSubmit} reducer={this.props.test}/>
        );
    }
}
Test.propTypes = {
	test: PropTypes.shape({
		data: PropTypes.arrayOf(PropTypes.string),
		isFetching: PropTypes.bool.isRequired
	}).isRequired,
	addNewItem: PropTypes.func.isRequired
};
const mapStateToProps = (state) => {
	return {
		test: state.test
	};
};
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		addNewItem
	}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(Test);



