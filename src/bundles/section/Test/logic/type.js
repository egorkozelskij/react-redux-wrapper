const TEST_REQUEST = 'test/request';
const TEST_SUCCESS = 'test/success';
const TEST_FAIL = 'test/fail';

export {
	TEST_REQUEST,
	TEST_SUCCESS,
	TEST_FAIL
};