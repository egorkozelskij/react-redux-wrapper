import * as actions from './action';

export const addNewItem = (item) => {
	return dispatch => {
		dispatch(actions.TestRequest());
		setTimeout(() => { //Эмуляция ассинхронности
			if (item === "error")
				dispatch(actions.TestFail("Some error"));
			dispatch(actions.TestSuccess(item));
		}, 1000)
	}
};