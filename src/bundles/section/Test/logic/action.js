import * as types from './type';

export const TestRequest = () => {
	return {
		type: types.TEST_REQUEST,
		payload: {
			isFetching: true
		}
	}
};

export const TestSuccess= (item) => {
	return {
		type: types.TEST_SUCCESS,
		payload: {
			isFetching: false,
			item: item
		}
	}
};

export const TestFail = (message) => {
	return {
		type: types.TEST_FAIL,
		payload: {
			isFetching: false,
			isSuccess: false,
			message: message
		}
	}
};