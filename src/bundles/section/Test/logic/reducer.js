import * as types from './type';

const initialState = {
	data: [
		"Item 1",
		"Item 2"
	],
	isFetching: false
};


function testReducer(state = initialState, action) {
	switch (action.type) {
		
		case types.TEST_REQUEST:
			return Object.assign({}, state, action.payload);
		
		case types.TEST_SUCCESS:
			return Object.assign({}, {
				isFetching: action.payload.isFetching,
				data: [...state.data, action.payload.item]
			});
		
		case types.TEST_FAIL:
			return Object.assign({}, state, action.payload);
			
		default:
			return state;
	}
}

export default testReducer;
