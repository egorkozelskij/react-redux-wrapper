const jssStyle = theme => ({
	formControl: {
		margin: theme.spacing.unit,
		minWidth: 120,
	},
	selectEmpty: {
		marginTop: theme.spacing.unit * 2,
	},
	myButton: {
		backgroundColor: theme.palette.secondary.dark,
		'&:hover': {
			backgroundColor: '#3f51b5 !important'
		}
	}
});
export default jssStyle;