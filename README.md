# react wrapper
### Client-React
- react + redux + thunk
- scss + jss + css
- Dev tools
	- React dev tools (browser ext)
	- React Hot loading
	- Redux dev tools (browser ext)

#### Run:
First setup:  **npm install** 
- Development: **npm run dev** (with hot reloading)
- Production: **npm run prod** (without hot reloading)


